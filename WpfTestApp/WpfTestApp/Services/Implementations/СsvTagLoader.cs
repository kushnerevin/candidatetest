﻿using System.Collections.Generic;
using WpfTestApp.Models;
using WpfTestApp.Services.Abstractions;

namespace WpfTestApp.Services.Implementations
{
    internal class СsvTagLoader : ITagLoader
    {
        public IEnumerable<TagData> Load(string fileName)
        {           
            return fastCSV.ReadFile<TagData>(
                fileName, 
                true,              
                ';',               
                (o, c) =>          
                {
                    o.Tag = c[0];
                    o.Type = c[1];
                    o.Address = fastCSV.ToInt(c[2]);                    
       
                    return true;
                });
        }
    }
}
