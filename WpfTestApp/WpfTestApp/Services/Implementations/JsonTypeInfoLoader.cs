﻿using System.IO;
using System.Text.Json;
using WpfTestApp.Models;
using WpfTestApp.Services.Abstractions;

namespace WpfTestApp.Services.Implementations
{
    internal class JsonTypeInfoLoader : ITypeInfoLoader
    {
        public TypeList Load(string fileName)
        {
            string jsonString = File.ReadAllText(fileName);
            return JsonSerializer.Deserialize<TypeList>(jsonString);            
        }
    }
}
