﻿using System.Collections.Generic;
using System.Xml;
using WpfTestApp.Models;
using WpfTestApp.Services.Abstractions;

namespace WpfTestApp.Services.Implementations
{
    internal class XMLDataSaver : IDataSaver
    {
        public void Save(string fileName, IEnumerable<ResultItem> results)
        {
            XmlWriter xmlWriter = XmlWriter.Create("result.xml");

            xmlWriter.WriteStartDocument();
            xmlWriter.WriteStartElement("root");

            foreach (var resultItem in results)
            {
                xmlWriter.WriteStartElement("item");
                xmlWriter.WriteAttributeString("Binding", "Introduced");

                xmlWriter.WriteStartElement("node-path");
                xmlWriter.WriteString(resultItem.Node);
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("address");
                xmlWriter.WriteString(resultItem.Address.ToString());
                xmlWriter.WriteEndElement();

                xmlWriter.WriteEndElement();
            }

            xmlWriter.WriteEndElement();
            xmlWriter.WriteEndDocument();
            xmlWriter.Close();
        }
    }
}
