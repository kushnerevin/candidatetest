﻿using System.Linq;
using WpfTestApp.Helpers;
using WpfTestApp.Models;
using WpfTestApp.Services.Abstractions;

namespace WpfTestApp.Services.Implementations
{
    internal class DataLoader : IDatatLoader
    {
        private readonly ITypeInfoLoader _typeInfoLoader;
        private readonly ITagLoader _tagLoader;       

        public string TypeInfosFileName { get; }

        public string TagsFileName { get; }

        public DataLoader(ITypeInfoLoader typeInfoLoader, ITagLoader tagLoader, IPropertiesService propertiesService)
        {
            _typeInfoLoader = typeInfoLoader;
            _tagLoader = tagLoader;            
            TypeInfosFileName = propertiesService.GetValue<string>(nameof(TypeInfosFileName));
            TagsFileName = propertiesService.GetValue<string>(nameof(TagsFileName));
        }

        public ResultItem[] Load()
        {            
            var typeList = _typeInfoLoader.Load(TypeInfosFileName);
            var tags = _tagLoader.Load(TagsFileName);

            var results = from t in tags
                       from i in typeList.TypeInfos where t.Type == i.TypeName
                       from p in i.Propertys
                       select new ResultItem { Node = $"{t.Tag}.{p.Key}", Address = SizeOfType.GetSize(p.Value) };            

            return results.ToArray();
        }
    }
}
