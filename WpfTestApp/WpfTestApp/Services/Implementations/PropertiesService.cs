﻿using WpfTestApp.Services.Abstractions;

namespace WpfTestApp.Services.Implementations
{
    internal class PropertiesService : IPropertiesService
    {
        public T GetValue<T>(string key)
        {
            return (T)Properties.Settings.Default[key];
        }
    }
}
