﻿using WpfTestApp.Models;

namespace WpfTestApp.Services.Abstractions
{
    public interface ITypeInfoLoader
    {
        TypeList Load(string fileName);
    }
}
