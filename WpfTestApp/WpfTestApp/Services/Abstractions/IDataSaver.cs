﻿using System.Collections.Generic;
using WpfTestApp.Models;

namespace WpfTestApp.Services.Abstractions
{
    public interface IDataSaver
    {
        void Save(string fileName, IEnumerable<ResultItem> results);
    }
}
