﻿namespace WpfTestApp.Services.Abstractions
{
    public interface IPropertiesService
    {
        T GetValue<T>(string key);
    }
}
