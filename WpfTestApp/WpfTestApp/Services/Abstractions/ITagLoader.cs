﻿using System.Collections.Generic;
using WpfTestApp.Models;

namespace WpfTestApp.Services.Abstractions
{
    public interface ITagLoader
    {
        IEnumerable<TagData> Load(string fileName);
    }
}
