﻿using WpfTestApp.Models;

namespace WpfTestApp.Services.Abstractions
{
    public interface IDatatLoader
    {
        ResultItem[] Load();
    }
}
