﻿using System.Collections.Generic;

namespace WpfTestApp.Models
{
    public class TypeInfo
    {
        public string TypeName { get; set; }

        public Dictionary<string, string> Propertys { get; set; }
    }
}
