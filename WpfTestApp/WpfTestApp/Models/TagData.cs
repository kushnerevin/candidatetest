﻿namespace WpfTestApp.Models
{
    public class TagData
    {
        public string Tag { get; set; }

        public string Type { get; set; }

        public int Address { get; set; }
    }
}
