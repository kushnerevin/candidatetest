﻿using System.Collections.Generic;

namespace WpfTestApp.Helpers
{
    internal class SizeOfType
    {
        private static readonly Dictionary<string, int> TypeSizes = new ()
        {
            { "double", 8 },
            { "bool", 1 },
            { "int", 4 },            
        };

        public static int GetSize(string typeName)
        {
            return TypeSizes[typeName];
        }
    }
}
