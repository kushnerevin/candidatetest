﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using WpfTestApp.Commands;
using WpfTestApp.Models;
using WpfTestApp.Services.Abstractions;

namespace WpfTestApp.ViewModels
{
    internal class MainWindowViewModel : ViewModelBase
    {
        private readonly IDatatLoader _resultLoader;  
        private readonly IDataSaver _dataSaver;

        public MainWindowViewModel(IDatatLoader resultLoader, IDataSaver dataSaver, IPropertiesService propertiesService)
        {
            _resultLoader = resultLoader;
            _dataSaver = dataSaver;
            ResultFileName = propertiesService.GetValue<string>(nameof(ResultFileName));
        }

        public string ResultFileName { get; }

        private string _message;

        public string Message 
        {
            get => _message;
            set 
            {
                _message = value;
                OnPropertyChanged(nameof(Message));
            } 
        }

        public ObservableCollection<ResultItem> Results { get; private set; } = new ObservableCollection<ResultItem>();
        

        private ICommand _loadInputDataCommand;

        public ICommand LoadInputDataCommand => _loadInputDataCommand ??= new RelayCommand(obj =>
        {
            Results.Clear();

            try
            {
                ResultItem[] results = _resultLoader.Load();

                if (results != null && results.Length > 0)
                {
                    int prevAddress = 0;

                    for (int i = 0; i < results.Length; i++)
                    {
                        var temp = results[i].Address;
                        results[i].Address = prevAddress;
                        prevAddress += temp;
                        Results.Add(results[i]);
                    }
                }
                Message = "Данные загружены";
            }
            catch (Exception ex)
            {
                Message = ex.Message;                
            }                        
        });

        private ICommand _saveResultsCommand;

        public ICommand SaveResultsCommand => _saveResultsCommand ??= new RelayCommand(obj =>
        {
            IList results = (IList)obj;

            if (results != null && results.Count > 0)
            {
                IEnumerable<ResultItem> resultItems = results.Cast<ResultItem>();

                try
                {
                    _dataSaver.Save(ResultFileName, resultItems);
                    Message = "Данные сохранены";
                }
                catch (Exception ex)
                {
                    Message = ex.Message;                    
                }                
            }
            else
            {
                Message = "Не выбраны данные для сохранения";
            }
        });
    }
}
