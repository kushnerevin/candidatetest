﻿using System.Windows;
using WpfTestApp.Services.Abstractions;
using WpfTestApp.Services.Implementations;
using WpfTestApp.ViewModels;

namespace WpfTestApp
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            ITypeInfoLoader typeInfoLoader = new JsonTypeInfoLoader();
            ITagLoader tagLoader = new СsvTagLoader();
            IPropertiesService propertiesService = new PropertiesService();
            IDatatLoader resultLoader = new DataLoader(typeInfoLoader, tagLoader, propertiesService);
            IDataSaver dataSaver = new XMLDataSaver();

            MainWindowViewModel viewModel = new(resultLoader, dataSaver, propertiesService);
            MainWindow window = new() { DataContext = viewModel };
            window.Show();
        }
    }
}
